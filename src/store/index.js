import { defineStore } from 'pinia';
import { getVaults } from "@/api/khronos";

export const useKhronosStore = defineStore('khronos', {
  state: () => ({
    availableTokens: ["LVtdcSgQcxRWqeMJhDTcjJGPBmfZYKTFu86ujUZevjW"], //BTC
    usdcToken: "BaTgvQyjijS18rFQzwd69c8akQKnoZYZowU3PKFggaRP",
    count: 0,
    vaults: [],
    lastRefresh: 0,
    workspace: {
      wallet: null,
      connection: null,
      provider: null,
      program: null,
    },
  }),
  getters: {
    getMultipledCount(state) {
      return state.count * 2
    },
    getActiveVaults(state) {
      return state.vaults.filter(v => v.account.isActive);
    },
    getClosedVaults(state) {
      return state.vaults.filter(v => !v.account.isActive);
    },
    isConnected(state) {
      return state.workspace.wallet != null;
    }
  },
  actions: {
    setupWorkspace(wallet, connection, provider, program) {
      this.workspace = {
        wallet: wallet,
        connection: connection,
        provider: provider,
        program: program,
      }
    },
    async refresh() {
      console.log('Time to Refresh!')
      // Todo: pridať hook/useCase ktorý bude komunikovať s api a upraví data do použitelného formátu pre view
      // ! Nevolať api priamo zo store
      if (this.workspace.wallet) {
        this.vaults = await getVaults(this.workspace);
      }
      else {
        this.vaults = [];
      }
      this.lastRefresh = Date.now();
    },
    incrementCounter(value) {
      this.count += value
    },
  },
});
