import * as anchor from "@project-serum/anchor";
import * as splToken from "@solana/spl-token";


export const closeVault = async ({ program, wallet, connection }, vaultPubKey) => {
    let vault = await program.account.vault.fetch(vaultPubKey);

    const vault_authority_pda = await anchor.web3.PublicKey.findProgramAddress(
        [Buffer.from(anchor.utils.bytes.utf8.encode("vault_seed"))],
        program.programId
    );

    const vault_deposit_account_pda = await anchor.web3.PublicKey.findProgramAddress(
        [
            Buffer.from(anchor.utils.bytes.utf8.encode("deposit")),
            vaultPubKey.toBuffer()
        ],
        program.programId
    );

    const vault_destination_account_pda = await anchor.web3.PublicKey.findProgramAddress(
        [
            Buffer.from(anchor.utils.bytes.utf8.encode("destination")),
            vaultPubKey.toBuffer()
        ],
        program.programId
    );

    let setupInstructions = [];

    let deposit_withdrawn_token_account = await splToken.Token.getAssociatedTokenAddress(
        splToken.ASSOCIATED_TOKEN_PROGRAM_ID, // always ASSOCIATED_TOKEN_PROGRAM_ID
        splToken.TOKEN_PROGRAM_ID, // always TOKEN_PROGRAM_ID
        vault.depositMint, // mint
        wallet.publicKey // owner
    );
    let deposit_withdrawn_acc_info = await connection.getAccountInfo(deposit_withdrawn_token_account);
    if (deposit_withdrawn_acc_info == null) {
        setupInstructions.push(
            splToken.Token.createAssociatedTokenAccountInstruction(
                splToken.ASSOCIATED_TOKEN_PROGRAM_ID, // always ASSOCIATED_TOKEN_PROGRAM_ID
                splToken.TOKEN_PROGRAM_ID, // always TOKEN_PROG
                vault.depositMint,
                deposit_withdrawn_token_account,
                wallet.publicKey,
                wallet.publicKey
            )
        )
    }

    let destination_withdrawn_token_account = await splToken.Token.getAssociatedTokenAddress(
        splToken.ASSOCIATED_TOKEN_PROGRAM_ID, // always ASSOCIATED_TOKEN_PROGRAM_ID
        splToken.TOKEN_PROGRAM_ID, // always TOKEN_PROGRAM_ID
        vault.destinationMint, // mint
        wallet.publicKey // owner
    );
    
    let destination_withdrawn_acc_info = await connection.getAccountInfo(destination_withdrawn_token_account);
    if (destination_withdrawn_acc_info == null) {
        setupInstructions.push(
            splToken.Token.createAssociatedTokenAccountInstruction(
                splToken.ASSOCIATED_TOKEN_PROGRAM_ID, // always ASSOCIATED_TOKEN_PROGRAM_ID
                splToken.TOKEN_PROGRAM_ID, // always TOKEN_PROG
                vault.destinationMint,
                destination_withdrawn_token_account,
                wallet.publicKey,
                wallet.publicKey
            )
        )
    }

    let trxSignature = await program.rpc.closeVault({
        accounts: {
            depositAccount: vault_deposit_account_pda[0],
            destinationAccount: vault_destination_account_pda[0],
            vault: vaultPubKey,
            depositWithdrawnAccount: deposit_withdrawn_token_account,
            destinationWithdrawnAccount: destination_withdrawn_token_account,
            authority: wallet.publicKey,
            vaultAuthority: vault_authority_pda[0],
            systemProgram: anchor.web3.SystemProgram.programId,
            tokenProgram: splToken.TOKEN_PROGRAM_ID,
            rent: anchor.web3.SYSVAR_RENT_PUBKEY,
        },
        preInstructions: setupInstructions
    });

    return trxSignature;
}