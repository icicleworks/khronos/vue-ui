import * as anchor from "@project-serum/anchor";

// TODO: create destination withdraw account

export const createVault = async ({ wallet, program }, period, percentil, amount, depositMint, destinationMint, initializerDepositAcc) => {
    const vault_account = anchor.web3.Keypair.generate()

    const [vault_deposit_account_pda, vault_deposit_account_bump] = await anchor.web3.PublicKey.findProgramAddress(
        [
            Buffer.from(anchor.utils.bytes.utf8.encode("deposit")),
            vault_account.publicKey.toBuffer()],
        program.programId
    );

    const [vault_destination_account_pda, vault_destination_account_bump] = await anchor.web3.PublicKey.findProgramAddress(
        [
            Buffer.from(anchor.utils.bytes.utf8.encode("destination")),
            vault_account.publicKey.toBuffer()],
        program.programId
    );

    const trxSignature = await program.rpc.initializeVault(period, percentil, new anchor.BN(amount), vault_deposit_account_bump, vault_destination_account_bump, {
        accounts: {
            depositAccount: vault_deposit_account_pda,
            destinationAccount: vault_destination_account_pda,
            vault: vault_account.publicKey,
            authority: wallet.publicKey,
            depositMint: new anchor.web3.PublicKey(depositMint),
            destinationMint: new anchor.web3.PublicKey(destinationMint),
            ownerDepositTokenAccount: initializerDepositAcc,
            rent: anchor.web3.SYSVAR_RENT_PUBKEY,
            systemProgram: anchor.web3.SystemProgram.programId,
            tokenProgram: new anchor.web3.PublicKey("TokenkegQfeZyiNwAJbNbGKPFXCWuBvf9Ss623VQ5DA"),
        },
        signers: [vault_account]
    });

    return trxSignature;
}