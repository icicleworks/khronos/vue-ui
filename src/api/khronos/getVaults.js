export const getVaults = async ({ program, wallet }) => {
    let filter = {
        memcmp: {
            offset: 8, // Discriminator.
            bytes: wallet.publicKey.toBase58(), //? For test: BAJ3zMPu52qNpPd1idnM3UJGQdK42qMQx2Qzg1b8VKHE
        }
    }
    return await program.account.vault.all([filter]);
}