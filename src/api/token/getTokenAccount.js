import axios from "axios";

export const getTokenAccount = async ({ wallet }, mint) => {
    let response = await axios({
        url: `https://api.devnet.solana.com`,
        method: "post",
        headers: { "Content-Type": "application/json" },
        data: [
            {
                jsonrpc: "2.0",
                id: 1,
                method: "getTokenAccountsByOwner",
                params: [
                    wallet.publicKey.toBase58(),
                    {
                        mint: mint,
                    },
                    {
                        encoding: "jsonParsed",
                    },
                ],
            }
        ]
    })

    console.log(response.data[0].result.value[0].account.data.parsed.info.tokenAmount)
    return response.data[0].result.value[0].pubkey;
}
