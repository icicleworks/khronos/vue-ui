import { web3 } from '@project-serum/anchor'

export const getAssociatedTokenAccounts = async ({ connection }, walletAddress) => {
    const accounts = await connection.getParsedProgramAccounts(
        new web3.PublicKey("TokenkegQfeZyiNwAJbNbGKPFXCWuBvf9Ss623VQ5DA"), // Todo -> config TOKEN_PROGRAM_ID
        {
            filters: [
                {
                    dataSize: 165, // number of bytes
                },
                {
                    memcmp: {
                        offset: 32, // number of bytes
                        bytes: walletAddress, // base58 encoded string
                    },
                },
            ],
        })
    return accounts;
}
