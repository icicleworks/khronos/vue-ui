import * as anchor from "@project-serum/anchor";

export const getVaultDepositAccount = async ({ program }, vault_account) => {
    const result = await anchor.web3.PublicKey.findProgramAddress(
        [
            Buffer.from(anchor.utils.bytes.utf8.encode("deposit")),
            vault_account.toBuffer()],
        program.programId
    );
    return result;
}

export const getVaultDestinationAccount = async ({ program }, vault_account) => {
    const result = await anchor.web3.PublicKey.findProgramAddress(
        [
            Buffer.from(anchor.utils.bytes.utf8.encode("destination")),
            vault_account.toBuffer()],
        program.programId
    );
    return result;
}