import { getVaultDepositAccount, getVaultDestinationAccount } from "@/utils";
import { getTokenBalance } from "@/api/token";

export const getVaultBalances = async (workspace, vault) => {
    let depositAccount = await getVaultDepositAccount(workspace, vault.publicKey);
    let destinationAccount = await getVaultDestinationAccount(workspace, vault.publicKey);

    let depositAmount = await getTokenBalance(workspace, depositAccount[0])
    let destinationAmount = await getTokenBalance(workspace, destinationAccount[0])

    return {
        depositAmount: depositAmount,
        destinationAmount: destinationAmount
    };
}