import { getVaultDepositAccount, getVaultDestinationAccount } from "@/utils";
import { getAccountTransactions } from "@/api/token";

export const getVaultHistory = async (workspace, vault) => {
    let depositAccount = await getVaultDepositAccount(workspace, vault.publicKey);
    let destinationAccount = await getVaultDestinationAccount(workspace, vault.publicKey);
    let transactions = await getAccountTransactions(workspace, depositAccount[0]);

    return transactions.map(trx => calcTokenDifferenceAmount(trx, depositAccount[0], destinationAccount[0]));
}

const calcTokenDifferenceAmount = (trx, depositAccount, destinationAccount) => {
    let depositIndex = trx.transaction.message.accountKeys.findIndex(i => i.toBase58() == depositAccount.toBase58());
    let destinationIndex = trx.transaction.message.accountKeys.findIndex(i => i.toBase58() == destinationAccount.toBase58());

    let depositBefore = trx.meta.preTokenBalances.find(b => b.accountIndex == depositIndex);
    let depositAfter = trx.meta.postTokenBalances.find(b => b.accountIndex == depositIndex);
    let depositAmountBefore = depositBefore != null ? depositBefore.uiTokenAmount.amount : 0;
    let depositAmountAfter = depositAfter != null ? depositAfter.uiTokenAmount.amount : 0;

    let destinationBefore = trx.meta.preTokenBalances.find(b => b.accountIndex == destinationIndex);
    let destinationAfter = trx.meta.postTokenBalances.find(b => b.accountIndex == destinationIndex);
    let destinationAmountBefore = destinationBefore != null ? destinationBefore.uiTokenAmount.amount : 0;
    let destinationAmountAfter = destinationAfter != null ? destinationAfter.uiTokenAmount.amount : 0;

    return {
        depositAccount: depositAccount,
        destinationAccount: destinationAccount,
        depositAmountBefore: depositAmountBefore,
        depositAmountAfter: depositAmountAfter,
        destinationAmountBefore: destinationAmountBefore,
        destinationAmountAfter: destinationAmountAfter,
        depositMintDecimals: depositAfter != null ? depositAfter.uiTokenAmount.decimals : depositBefore != null ? depositBefore.uiTokenAmount.decimals : 6,
        destinationMintDecimals: destinationAfter != null ? destinationAfter.uiTokenAmount.decimals : destinationBefore != null ? destinationBefore.uiTokenAmount.decimals : 6,
        time: trx.blockTime
    }
}
