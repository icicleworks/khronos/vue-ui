import 'solana-wallets-vue/styles.css';

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createPinia } from 'pinia'
import SolanaWallets from 'solana-wallets-vue';
import { WalletAdapterNetwork } from "@solana/wallet-adapter-base"
import {
  PhantomWalletAdapter,
  SlopeWalletAdapter,
  SolflareWalletAdapter,
} from '@solana/wallet-adapter-wallets';
import veProgress from 'vue-ellipse-progress';

// Todo add other wallets
const walletOptions = {
  wallets: [
    new PhantomWalletAdapter(),
    new SlopeWalletAdapter(),
    new SolflareWalletAdapter(
      { network: WalletAdapterNetwork.Devnet } // Todo
    ),
  ],
  autoConnect: true,
}

createApp(App)
  .use(createPinia())
  .use(SolanaWallets, walletOptions)
  .use(router)
  .use(veProgress)
  .mount('#app')
