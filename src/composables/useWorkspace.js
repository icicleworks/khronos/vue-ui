import { computed } from 'vue'
import { useAnchorWallet } from 'solana-wallets-vue'
import { Connection, PublicKey } from '@solana/web3.js'
import { Provider, Program } from '@project-serum/anchor'
import idl from '../../idl/khronos.json'

const programID = new PublicKey(idl.metadata.address)
const opts = {
    preflightCommitment: "processed"
}

export const initWorkspace = (store) => {
    const wallet = useAnchorWallet()
    const connection = new Connection('https://api.devnet.solana.com', opts.preflightCommitment)
    const provider = computed(() => new Provider(connection, wallet.value, opts.preflightCommitment))
    const program = computed(() => new Program(idl, programID, provider.value))

    store.setupWorkspace(
        wallet,
        connection,
        provider,
        program
    )
}
